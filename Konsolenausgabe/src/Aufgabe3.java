
public class Aufgabe3 {

	public static void main(String[] args) {
		System.out.printf("%12s", "Fahrenheit");
		System.out.printf("%4s", "|");
		System.out.printf("%10s", "Celsius");
		
		
		System.out.printf("%s", "\n  -------------------------");
		
		
		System.out.printf("\n%5s", "-20");
		System.out.printf("%11s","|");
		System.out.printf("%11s", "-28.89");
		
		
		System.out.printf("\n%5s", "-10");
		System.out.printf("%11s","|");
		System.out.printf("%11s", "-23.33");
		
		System.out.printf("\n%4s", "+0");
		System.out.printf("%12s","|");
		System.out.printf("%11s", "-17.78");
		
		
		System.out.printf("\n%5s", "+20");
		System.out.printf("%11s","|");
		System.out.printf("%11s", "-6.67");
		
		
		System.out.printf("\n%5s", "+10");
		System.out.printf("%11s","|");
		System.out.printf("%11s", "-1.11");

	}

}
