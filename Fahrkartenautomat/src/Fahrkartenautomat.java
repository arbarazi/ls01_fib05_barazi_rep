﻿import java.text.DecimalFormat;
import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
//       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
//       double eingezahlterGesamtbetrag;
       double rückgabebetrag;
//       int Ticketsanzahl;
       double y;
       
       
       zuZahlenderBetrag= fahrkartenbestellungErfassen();
       
       DecimalFormat format = new DecimalFormat("0.00");
       
       
       // Geldeinwurf 
       // -----------
//       eingezahlterGesamtbetrag = 0.0;
//       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
//       {
//    	   System.out.println("Noch zu zahlen: " + ("zuZahlenderBetrag: " + format.format(new Double(zuZahlenderBetrag) - eingezahlterGesamtbetrag)));
//    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
//    	   eingeworfeneMünze = tastatur.nextDouble();
//           eingezahlterGesamtbetrag += eingeworfeneMünze;
//       }
       
       rückgabebetrag=fahrkartenBezahlen(zuZahlenderBetrag);
       
       
//
//       // Fahrscheinausgabe
//       // -----------------  
//       System.out.println("\nFahrschein wird ausgegeben");
//       for (int i = 0; i < 8; i++)
//       {
//          System.out.print("=");
//          try {
//			Thread.sleep(250);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//       }
//       System.out.println("\n\n");
       
       
       fahrkartenAusgeben();
       
       
       

       
       
       // Rückgeldberechnung und -Ausgabe
       // ------------------------------
     
//       if(rückgabebetrag > 0.0)
//       {
//    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
//    	   System.out.println("wird in folgenden Münzen ausgezahlt:");
//
//           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
//           {
//        	  System.out.println("2 EURO");
//	          rückgabebetrag -= 2.0;
//           }
//           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
//           {
//        	  System.out.println("1 EURO");
//	          rückgabebetrag -= 1.0;
//           }
//           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
//           {
//        	  System.out.println("50 CENT");
//	          rückgabebetrag -= 0.5;
//           }
//           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
//           {
//        	  System.out.println("20 CENT");
// 	          rückgabebetrag -= 0.2;
//           }
//           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
//           {
//        	  System.out.println("10 CENT");
//	          rückgabebetrag -= 0.1;
//           }
//           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
//           {
//        	  System.out.println("5 CENT");
// 	          rückgabebetrag -= 0.05;
//           }
//       }

       
//       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
//                          "vor Fahrtantritt entwerten zu lassen!\n"+
//                          "Wir wünschen Ihnen eine gute Fahrt.");
       
       
       y= rueckgeldAusgeben(rückgabebetrag);
       
    }
    
    
    
    	public static double fahrkartenbestellungErfassen () {
    		
    		 Scanner tastatur = new Scanner(System.in);
    	      
    	       double zuZahlenderBetrag_; 
    	       int Ticketsanzahl_;
    	       double x;

    	       
    	       System.out.println("Zu Zahlen ist:");
    	       zuZahlenderBetrag_ = tastatur.nextDouble();
    	      
    	       System.out.println("Anzahl der Tickets");
    	       Ticketsanzahl_= tastatur.nextInt();
    	       
    	       zuZahlenderBetrag_= Ticketsanzahl_*zuZahlenderBetrag_;
    	       
    	       x= zuZahlenderBetrag_;
    	       
    	       
    	       return x ; 
    	}
    	
    	
    	
    	
    	public static double fahrkartenBezahlen(double x) {
    		Scanner tastatur = new Scanner(System.in);
    		double eingeworfeneMünze;
    		double rückgabebetrag;
    		double eingezahlterGesamtbetrag;
    		
    		DecimalFormat format = new DecimalFormat("0.00");
    		
    		eingezahlterGesamtbetrag = 0.0;
    	       while(eingezahlterGesamtbetrag < x)
    	       {
    	    	   System.out.println("Noch zu zahlen: " + ("zuZahlenderBetrag: " + format.format(new Double(x) - eingezahlterGesamtbetrag)));
    	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	    	   eingeworfeneMünze = tastatur.nextDouble();
    	           eingezahlterGesamtbetrag += eingeworfeneMünze;
    	       }
    	       rückgabebetrag = eingezahlterGesamtbetrag - x;
    		return rückgabebetrag; 
    	}
    	
    	
    	
    	
    	public static void fahrkartenAusgeben() {
    	
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");	
    	}
    	
    	
    	
    	
    	
    	public static double rueckgeldAusgeben(double x) {

    	       if(x > 0.0)
    	       {
    	    	   System.out.println("Der Rückgabebetrag in Höhe von " + x + " EURO");
    	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

    	           while(x >= 2.0) // 2 EURO-Münzen
    	           {
    	        	  System.out.println("2 EURO");
    		          x-= 2.0;
    	           }
    	           while(x >= 1.0) // 1 EURO-Münzen
    	           {
    	        	  System.out.println("1 EURO");
    		          x -= 1.0;
    	           }
    	           while(x >= 0.5) // 50 CENT-Münzen
    	           {
    	        	  System.out.println("50 CENT");
    		          x -= 0.5;
    	           }
    	           while(x >= 0.2) // 20 CENT-Münzen
    	           {
    	        	  System.out.println("20 CENT");
    	 	          x -= 0.2;
    	           }
    	           while(x >= 0.1) // 10 CENT-Münzen
    	           {
    	        	  System.out.println("10 CENT");
    		          x -= 0.1;
    	           }
    	           while(x >= 0.05)// 5 CENT-Münzen
    	           {
    	        	  System.out.println("5 CENT");
    	 	          x -= 0.05;
    	 	          
    	           }
    	       }
    	       
    	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir wünschen Ihnen eine gute Fahrt.");
    	       return x;

    	}
    
}